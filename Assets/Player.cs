﻿using System;
using UnityEngine;

public enum PlayerType {one,two}

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerType playerType;
    private CharacterController _characterController;
    [SerializeField] private float _speed = .1f;
    private float _gravity = 30;

    [SerializeField] private Transform handle;

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        var moveDirection = Vector3.zero;

        if (_characterController.isGrounded)
        {

            if (playerType == PlayerType.one)
                moveDirection = new Vector3(Input.GetAxis("HorizontalOne") * _speed, 0.0f, 0.0f);
            if (playerType == PlayerType.two)
                moveDirection = new Vector3(Input.GetAxis("HorizontalTwo") * _speed, 0.0f, 0.0f);
        }

        moveDirection.y -= _gravity * Time.deltaTime;
        
        
        _characterController.Move(moveDirection * Time.deltaTime);

//        if (handle)
//            handle.transform.position = transform.position;
    }

    
}
