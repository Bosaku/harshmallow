﻿using CurvedLineRenderer;
using UnityEngine;

namespace InputAndUI
{
     [RequireComponent(typeof(CurvedLineRenderer.CurvedLineRenderer))]
     public class EntityToGraphicBezier : MonoBehaviour
     {
          //cleanup from TDD 
          public Transform worldTransformToFollow;
          public RectTransform graphicToGoTo;
          public Camera cam;

          public CurvedLinePoint[] points;

          public float lerpSpeedMulti = 3;
          public Vector3 midpointOffset = new Vector3(0, 1, 0);
          public Vector3 endpointOffset = Vector3.zero;
          

//     public Rect rect;
          private void OnValidate()
          {
               points = GetComponentsInChildren<CurvedLinePoint>();
               cam = FindObjectOfType<Camera>();
          }

          private void OnEnable()
          {
               points = GetComponentsInChildren<CurvedLinePoint>();
               cam = FindObjectOfType<Camera>();
          }

          private void Update()
          {
               if (graphicToGoTo && worldTransformToFollow && cam && points[0] && points[1] && points[2])
               {
                    Vector3 result = Vector3.zero;

//               myTransform.position = Vector3.Lerp(myTransform.position, graphicToGoTo.transform.position, t * lerpSpeedMulti);

                    points[0].transform.position = Vector3.Lerp(points[0].transform.position, worldTransformToFollow.position, Time.deltaTime * lerpSpeedMulti);
                    points[2].transform.position = Vector3.Lerp(points[2].transform.position, cam.ScreenToWorldPoint(graphicToGoTo.position + endpointOffset), Time.deltaTime * lerpSpeedMulti);
                    points[1].transform.position = Vector3.Lerp (points[1].transform.position, ((points[0].transform.position + points[2].transform.position) / 2) + midpointOffset, Time.deltaTime * lerpSpeedMulti);
               }
          }

          [ContextMenu("Line it up")]
          private void SetUpLine()
          {
               if (graphicToGoTo && worldTransformToFollow && cam && points[0] && points[1] && points[2])
               {
                    Vector3 result = Vector3.zero;

//               myTransform.position = Vector3.Lerp(myTransform.position, graphicToGoTo.transform.position, t * lerpSpeedMulti);

                    points[0].transform.position = worldTransformToFollow.position;
                    points[2].transform.position = cam.ScreenToWorldPoint(graphicToGoTo.position + endpointOffset);
                         //RectTransformUtility.WorldToScreenPoint(cam, graphicToGoTo.transform.position);
                    points[1].transform.position = (points[0].transform.position + points[2].transform.position) / 2;
               }
          }
     }
}