//using System;
//using DigitalRuby.Tween;
using UnityEngine;

namespace InputAndUI
{
    public class Bezier : MonoBehaviour
    {
        public Transform handleOne;
        //public Transform handleTwo;
        public Transform handleThree;

        public void Initialize(Transform pointingTo, Transform endPoint)
        {
            handleOne = transform.Find("1");
            //handleTwo = transform.Find("2");
            handleThree = transform.Find("3");

            transform.parent = pointingTo;
            handleOne.position = pointingTo.position;


            handleThree.parent = endPoint;
            handleThree.position = endPoint.position;
          //  handleTwo.position = (handleOne.position + handleThree.position)/2; 

//            var mat = GetComponent<MeshRenderer>().materials[0];

//            TweenFactory.Tween(mat, mat.color, 0,3, TweenScaleFunctions.QuadraticEaseOut, (t) =>
//            {
//                Debug.Log(t);
//                //PoolObject.Destroy(gameObject);
//            });

            var lineRenderer = GetComponent<LineRenderer>();
            var mat = lineRenderer.material;

//            handleOne.gameObject.Tween(gameObject, mat.color, new Color(mat.color.r, mat.color.g, mat.color.b, 0), 
//                1,TweenScaleFunctions.QuadraticEaseOut,
//                (t) =>
//                {
//                    mat.color = t.CurrentValue;
////                    if (t.CurrentValue.a == 0)
////                    {
////                        PoolObject.Destroy(gameObject);
////                    }
//                });

        }
    }
}